package lineCountFromInputFolder;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class FolderLineCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

    int count = 0;

    //increment count for each node
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        this.count++;
    }

    //after completion of reduce operation, increment counter by total no of nodes
    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.getCounter("Result", "Result").increment(count);
    }

}
