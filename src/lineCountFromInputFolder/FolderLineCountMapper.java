package lineCountFromInputFolder;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author Devdatta
 *
 */
/*
 * This will simply output 1 for each line in a file and key will be same for all lines
 */
public class FolderLineCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private static final Pattern DATA = Pattern.compile("\\[\\[(.*?)\\]\\]");
    Pattern TITLE = Pattern.compile("<title>(.*?)</title>");

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String line = value.toString();
        line = line.trim();
        String title = null;
        Matcher matcher2 = TITLE.matcher(line);
        //find node in title
        if (matcher2.find()) {
            title = matcher2.group();
            title = title.trim();
            title = title.substring(7, title.length() - 8);
            //write it to context
            context.write(new Text(title), one);
        }
        
        //find nodes in text
        Matcher matcher = DATA.matcher(line);
        while (matcher.find()) {
            String outlink = matcher.group();
            outlink = outlink.trim();
            outlink = outlink.substring(2, outlink.lastIndexOf(']') - 1);
            //write it to context
            context.write(new Text(outlink), one);
        }
    }

    private final static IntWritable one = new IntWritable(1);
//    
//    
//    private final static IntWritable one = new IntWritable(1);
//
//    @Override
//    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//        //skip empty lines and lines that do not have title tag
//        if (value.toString().equals("") || !value.toString().contains("<title>") 
//                || !value.toString().contains("</title>")) {
//            return;
//        }
//        context.write(new Text(docsInWiki), one);
////        context.write(new Text(docsInWikiForCount), one);
//    }
//
//    

}
