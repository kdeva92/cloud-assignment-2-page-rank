package lineCountFromInputFolder;

import static cloudassignment2.CloudAssignment2.setSeparator;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

/**
 *
 * @author deva
 */
public class FolderLineCountJob extends Configured implements Tool {

    String outputPath, inputPath;

    public FolderLineCountJob(String inputPath, String outputPath) {
        this.inputPath = inputPath;
        this.outputPath = outputPath;
    }

    @Override
    public int run(String[] strings) throws Exception {
//        Configuration configuration = getConf();
        Job job = Job.getInstance(getConf(), "linecount");
        job.setJarByClass(this.getClass());
        setSeparator(job.getConfiguration());
        FileInputFormat.addInputPaths(job, inputPath);
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        //this will make sure single reducer is run for line count in doc
        job.setNumReduceTasks(1);
        job.setMapperClass(FolderLineCountMapper.class);
        job.setReducerClass(FolderLineCountReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        job.waitForCompletion(true);
        //get total no of nodes from job by reading from counter
        int totalFiles = (int) job.getCounters().findCounter("Result", "Result").getValue();
        System.out.println("\n\n**** " + totalFiles + "****\n\n");
        return totalFiles;
    }

}
