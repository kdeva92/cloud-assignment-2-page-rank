package pageRank;

import static cloudassignment2.CloudAssignment2.setSeparator;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

/**
 *
 * @author deva
 */

/*
    Job to do actual page rank computations, can be called multiple times for 
    multiple passes
*/
public class PageRankJob extends Configured implements Tool {

    String outputPath, inputPath;

    public PageRankJob(String inputPath, String outputPath) {
        this.inputPath = inputPath;
        this.outputPath = outputPath;
    }

    @Override
    public int run(String[] strings) throws Exception {
        Job job3;
        job3 = Job.getInstance(getConf(), "ranker");
        job3.setJarByClass(this.getClass());
        setSeparator(job3.getConfiguration());
        FileInputFormat.addInputPaths(job3, inputPath);
        FileOutputFormat.setOutputPath(job3, new Path(outputPath));
        job3.setMapperClass(PageRankMapper.class);
        job3.setReducerClass(PageRankReducer.class);
        job3.setOutputFormatClass(TextOutputFormat.class);
        job3.setMapOutputKeyClass(Text.class);
        job3.setMapOutputValueClass(Text.class);
        return job3.waitForCompletion(true) ? 0 : 1;
    }

}
