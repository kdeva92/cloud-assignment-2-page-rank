package pageRank;

import static cloudassignment2.CloudAssignment2.mapredKeySeparator;
import static cloudassignment2.CloudAssignment2.out_docId;
import static cloudassignment2.CloudAssignment2.separator;
import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author deva
 */

/*
    Mapper job for page rank computation
*/
public class PageRankMapper extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    protected void map(LongWritable key, Text value, Mapper.Context context) throws IOException, InterruptedException {
        //separate page name and details
        String[] data = value.toString().split(mapredKeySeparator);
        String page = data[0];
        //split to separate page rank and outlinks
        data = data[1].split(out_docId);
        double rank = Double.parseDouble(data[0]);
        String outlinks = "";
        if (data.length > 1) {
            outlinks = data[1];
            data = data[1].split(separator);
            double rankToGive = rank / data.length;
            for (String d : data) {
                //write contribution towards each outlink
                context.write(new Text(d), new Text("" + rankToGive));
            }
        }
        //write outlinks
        context.write(new Text(page), new Text(outlinks));
    }

}
