package pageRank;

import static cloudassignment2.CloudAssignment2.out_docId;
import static cloudassignment2.CloudAssignment2.separator;
import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author deva
 */
public class PageRankReducer extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        double rank = 0;
        String outlinks = "";
        //calculate new rank by adding all inputs
        for (Text value : values) {
            if (value.toString().contains(separator)) {
                outlinks = value.toString();
            } else if (!value.toString().equals("")) {
                rank += Double.parseDouble(value.toString());
            }
        }
        //calculate page rank by adding damping factor and write it out
        rank = .15 + .85 * rank;
        context.write(key, new Text(rank + out_docId + outlinks));
    }

}
