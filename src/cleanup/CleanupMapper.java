package cleanup;

import static cloudassignment2.CloudAssignment2.mapredKeySeparator;
import static cloudassignment2.CloudAssignment2.out_docId;
import java.io.IOException;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author deva
 */
/*
    This mapper will read page and its rank and write it onto context with rank as key
    and page as its value so that sorting can be done.
*/

public class CleanupMapper extends Mapper<LongWritable, Text, DoubleWritable, Text> {

    @Override
    protected void map(LongWritable key, Text value, Mapper.Context context) throws IOException, InterruptedException {
        String[] data = value.toString().split(mapredKeySeparator);
        String page = data[0];
        data = data[1].split(out_docId);
        double rank = Double.parseDouble(data[0]);
        context.write(new DoubleWritable(rank), new Text(page));
    }
}
