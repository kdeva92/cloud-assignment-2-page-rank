package cleanup;

import java.util.List;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

/**
 *
 * @author deva
 */

/*
    This class will run cleanup job, ie. it will remove all unnecessary outlinks,
    sort and filter top 100 entries
 */
public class CleanupJob extends Configured implements Tool {

    String outputPath, inputPath;
    List<String> foldersToClean;

    public CleanupJob(String inputPath, String outputPath, List<String> folderToClean) {
        this.inputPath = inputPath;
        this.outputPath = outputPath;
        this.foldersToClean = folderToClean;
    }

    @Override
    public int run(String[] strings) throws Exception {
        Job job4 = Job.getInstance(getConf(), "init");
        job4.setJarByClass(this.getClass());
        FileInputFormat.addInputPaths(job4, inputPath);
        FileOutputFormat.setOutputPath(job4, new Path(outputPath));
        //This is to force single reducer
        job4.setNumReduceTasks(1);
        job4.setMapperClass(CleanupMapper.class);
        job4.setReducerClass(CleanupReducer.class);
        job4.setSortComparatorClass(MyComparator.class);
        job4.setMapOutputKeyClass(DoubleWritable.class);
        job4.setMapOutputValueClass(Text.class);
        job4.waitForCompletion(true);

        boolean result = job4.waitForCompletion(true);
        //cleanup of all intermediate directories
        FileSystem fs = FileSystem.get(getConf());
        for (String path : foldersToClean) {
            Path pt = new Path(path);
            fs.delete(pt, true);
        }

        return result ? 0 : 1;
    }

}

/*
    This is comparator to be used for decending sort
 */
class MyComparator extends WritableComparator {

    protected MyComparator() {
        super(DoubleWritable.class, true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        DoubleWritable k1 = (DoubleWritable) a;
        DoubleWritable k2 = (DoubleWritable) b;
        return -1 * k1.compareTo(k2);
    }

}
