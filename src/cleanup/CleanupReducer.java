package cleanup;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author deva
 */
/*
    This reducer will output pages sorted by their ranks.
    We need to show only 100 pages hence the reducre will stop writing to context once
    100 pages are written
*/
public class CleanupReducer extends Reducer<DoubleWritable, Text, Text, DoubleWritable>  {

    int i = 0;
    @Override
    protected void reduce(DoubleWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        if(i<100){
            Iterator<Text> iterator = values.iterator();
            while(iterator.hasNext()){
                i++;
                context.write(iterator.next(), key);
            }
        }
    }    
}
