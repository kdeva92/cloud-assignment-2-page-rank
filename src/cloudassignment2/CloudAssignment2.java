package cloudassignment2;

import cleanup.CleanupJob;
import java.util.ArrayList;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;

/**
 *
 * @author Devdatta Kulkarni
 */
public class CloudAssignment2 {

    // All static variables below are for "keys" shared between mapped and reducers imported as static import at compile time.
    public static String docsInWiki = "#&#&DocumentsInWiki#&#&";
    public static String docsInWikiForCount = "#&#&_DocumentsInWikiForCount#&#&";
    public static String lineFlag = "Line";
    public static String in_docId = "#&#&in_docId#&#&";
    public static String out_docId = "#&#&out_docId#&#&";
    public static String separator = "#&#&#&#";
    public static String mapredKeySeparator = "###&&&&###";

    public static void main(String[] args) throws Exception {
        ArrayList<String> foldersToClean = new ArrayList<>();
        //int res = ToolRunner.run(new CloudAssignment2(), args);
        //System.exit(res);
        String input = args[0];
        String output = args[1];

        //call to line count
        int nodes = ToolRunner.run(new lineCountFromInputFolder.FolderLineCountJob(input, input + "_lineCount"), null);
        foldersToClean.add(input + "_lineCount");

        //call to init graph
        ToolRunner.run(new initGraph.InitGraphJob(input, input + "phase0", nodes), null);
        foldersToClean.add(input + "phase0");

        //call page rank in loop
        for (int i = 1; i <= 10; i++) {
            ToolRunner.run(new pageRank.PageRankJob(input + "phase" + (i - 1), input + "phase" + i), null);
            foldersToClean.add(input + "phase" + i);
        }

        //call to cleanup
        ToolRunner.run(new CleanupJob(input + "phase10", output, foldersToClean), null);
    }

    //function for settig separator for different configs
    public static void setSeparator(Configuration conf) {
        conf.set("mapred.textoutputformat.separator", mapredKeySeparator); //Prior to Hadoop 2 (YARN)
        conf.set("mapreduce.textoutputformat.separator", mapredKeySeparator);  //Hadoop v2+ (YARN)
        conf.set("mapreduce.output.textoutputformat.separator", mapredKeySeparator);
        conf.set("mapreduce.output.key.field.separator", mapredKeySeparator);
        conf.set("mapred.textoutputformat.separatorText", mapredKeySeparator);
    }
}
