package initGraph;

import static cloudassignment2.CloudAssignment2.out_docId;
import static cloudassignment2.CloudAssignment2.separator;
import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author Devdatta Kulkarni
 */

/*
    Reducer will generate initial rank and write it along ith outlinks of a node
*/
public class CreateGraphReducer extends Reducer<Text, Text, Text, Text> {

    double rank;

    //Read no of nodes from context
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context); 
        rank = context.getConfiguration().getDouble("IpFiles", 0);
        rank = 1.0 / rank;
    }

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        StringBuilder sb = new StringBuilder();
        Iterator<Text> it = values.iterator();
        // append all outlinks to each other
        while (it.hasNext()) {
            Text value = it.next();
            sb.append(value.toString());
                sb.append(separator);
        }
        //write page, rank and outlinks to file
        context.write(key, new Text(rank + out_docId + sb.toString()));

    }

}


/*
public class CreateGraphReducer extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        ArrayList<String> inlinks = new ArrayList<>();
//        ArrayList<String> oulinks = new ArrayList<>();

        for (Text value : values) {
            String[] data = value.toString().split(separator);
            if (data[0].equals(in_docId)) {
                inlinks.add(data[1]);
//            } else if (data[0].equals(out_docId)) {
//                oulinks.add(data[1]);
//            }
        }

        context.write(key, new Text(in_docId));
//        context.write(key, new Text(out_docId));

    }

}


 */
