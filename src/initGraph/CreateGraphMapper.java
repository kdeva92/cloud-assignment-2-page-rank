package initGraph;

import static cloudassignment2.CloudAssignment2.separator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author Devdatta Kulkarni
 *
 */

/*
 * Mapper to emit doc name as key and outlinks as value
 */
public class CreateGraphMapper extends Mapper<LongWritable, Text, Text, Text> {

    private static final Pattern DATA = Pattern.compile("\\[\\[.*?\\]\\]");
    Pattern TITLE = Pattern.compile("<title>(.*?)</title>");

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String line = value.toString();
        line = line.trim();
        String title;
        Matcher matcher2 = TITLE.matcher(line);
        //Extract title from line
        if (matcher2.find()) {
            title = matcher2.group();
            title = title.trim();
            title = title.substring(7, title.length() - 8);
        } else {
            //if no title, do nothing and return
            return;
        }

        //extract all outlinks based on pattern 
        List<String> outLinks = new ArrayList<>();
        Matcher matcher = DATA.matcher(line);
        while (matcher.find()) {
            String outlink = matcher.group();
            outlink = outlink.trim();
            outlink = outlink.substring(2, outlink.lastIndexOf(']') - 1);
            outlink = outlink.replace("[[", "");
            outLinks.add(outlink);
        }
        //create single string of all outlinks separated by separator
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < outLinks.size(); i++) {
            String data = outLinks.get(i);
            buffer.append(data);
            if (i != outLinks.size() - 1) {
                buffer.append(separator);
            }
        }
        
        //weite node name as key and <rank> separator <list of outlinks to context>
        context.write(new Text(title), new Text(buffer.toString()));
    }

}


/*
public class CreateGraphMapper extends Mapper<LongWritable, Text, Text, Text> {

    private static final Pattern DATA = Pattern.compile("\\[\\[.*?\\]\\]");
    Pattern TITLE = Pattern.compile("<title>(.*?)</title>");

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String line = value.toString();
        String title;
        Matcher matcher2 = TITLE.matcher(line);
        if (matcher2.find()) {
            title = matcher2.group();
            title = title.substring(7, title.length() - 8);
        } else {
            //no title for the document
            return;
        }

        Matcher matcher = DATA.matcher(line);
        while (matcher.find()) {
            String outlink = matcher.group();
            outlink = outlink.substring(2, outlink.lastIndexOf(']') - 1);
            //write as inlink of found inlink
//            context.write(new Text(outlink), new Text(in_docId+separator+title));
            context.write(new Text(title), new Text(out_docId+separator+outlink));
        }
    }

}



 */
