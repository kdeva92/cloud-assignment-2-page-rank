/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package initGraph;

import static cloudassignment2.CloudAssignment2.setSeparator;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

/**
 *
 * @author deva
 */
/*
    This job is for initializing graph as 
    node <separator> initial rank <separator> list of outlinks
 */
public class InitGraphJob extends Configured implements Tool {

    String outputPath, inputPath;
    int nodeCount;

    public InitGraphJob(String inputPath, String outputPath, int nodecount) {
        this.inputPath = inputPath;
        this.outputPath = outputPath;
        this.nodeCount = nodecount;
    }

    @Override
    public int run(String[] strings) throws Exception {
        Job job2 = Job.getInstance(getConf(), "initMap");
        job2.setJarByClass(this.getClass());
        setSeparator(job2.getConfiguration());
        FileInputFormat.addInputPaths(job2, inputPath);
        FileOutputFormat.setOutputPath(job2, new Path(outputPath));
        job2.getConfiguration().set("IpFiles", "" + nodeCount);
        job2.setMapperClass(CreateGraphMapper.class);
        job2.setReducerClass(CreateGraphReducer.class);
        job2.setMapOutputKeyClass(Text.class);
        job2.setOutputFormatClass(TextOutputFormat.class);
        job2.setMapOutputValueClass(Text.class);
        job2.waitForCompletion(true);
        return job2.waitForCompletion(true) ? 0 : 1;
    }

}
