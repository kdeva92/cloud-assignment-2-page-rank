from pyparsing import Regex, re
from pyspark import SparkContext,SparkConf
import sys

conf = SparkConf().setAppName("demo")
sc = SparkContext(conf=conf)

linkCnt=0.0

def createOutlinks(li):
        if not li:
                return
        else:
                title = re.search('\<title\>(.*?)\<\/title\>',li).group(1)
                data = re.findall('\[\[(.*?)\]\]',li)
                if not data:
                        data = []
                return (title,(1.0/linkCnt,data))


def distributeRank(entry):
        page = entry[0]
        rank = (entry[1])[0]
        outlinks = (entry[1])[1]
        listToRet = []
        listToRet.append((page,(0.0,outlinks)))
        if not outlinks:
                return listToRet
        rankToDist = rank/len(outlinks)
        for outlink in outlinks:
                listToRet.append((outlink,(rankToDist,"")))
        return listToRet

def reduceData(x,y):
        rank = x[0]+y[0]
        outlinks = []
        if x[1]:
                outlinks = x[1]
        if y[1]:
                outlinks = y[1]
        return (rank,outlinks)

inputFile = str(sys.argv[1])
outputDir = str(sys.argv[2])

#lines = sc.textFile("smallInput2/graph2.txt")
lines = sc.textFile(inputFile)
#lines = sc.textFile("input/simplewiki-20150901-pages-articles-processed.xml")
#lines = sc.textFile("input/graph2.txt")
titles = lines.flatMap(lambda x: re.findall('\<title\>(.*?)\<\/title\>',x))
datas = lines.flatMap(lambda x: re.findall('\[\[(.*?)\]\]',x)).distinct()
allLinks = titles.union(datas).distinct()
#allLinks.persist(StorageLevel.MEMORY_AND_DISK_SER)
linkCnt = allLinks.count()
#print op
dataMap = lines.map(lambda x: createOutlinks(x))
#print dataMap.collect()
#print dataMap.collect()
#actual PR calculation
for i in range(0, 10):
        mappedOp = dataMap.flatMap(lambda x : distributeRank(x))
        dataMap = mappedOp.reduceByKey(lambda x,y : reduceData(x,y))
        dataMap = dataMap.map(lambda x: (x[0],(x[1][0]*.85+.15 ,x[1][1])))
#print dataMap.collect()
print dataMap.count()

#dataMap.coalesce(1).saveAsTextFile("sparkAllData")
sc.parallelize(dataMap.coalesce(1).takeOrdered(100, key = lambda x: -x[1][0])).coalesce(1).map(lambda x: x[0]+str("   ")+str(x[1][0])).saveAsTextFile(outputDir)

